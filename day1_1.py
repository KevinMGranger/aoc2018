import sys
x = 0
for input_ in sys.stdin:
    sign, num = input_[0], int(input_[1:])
    if sign == "+":
        x += num
    elif sign == "-":
        x -= num
    else:
        raise ValueError

print(x)