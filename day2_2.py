#!/usr/bin/env python3

import sys
from collections import Counter


def pairs(it):
    it = iter(it)
    for first in it:
        second = next(it)
        yield first, second


def cksumify(string):
    cksum = [ord(x) for x in string]
    while True:
        yield cksum
        if len(cksum) == 1:
            break
        cksum = [x*y for x, y in pairs(cksum)]


for box in sys.stdin:
    counts = list(Counter(box).values())
    if 2 in counts:
        twofers += 1
    if 3 in counts:
        threefers += 1

print(twofers * threefers)