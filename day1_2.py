#!/usr/bin/env python3

import sys
from collections import Counter

x = 0
freqs = Counter((0,))
done = False

freq_list = [(line[0], int(line[1:])) for line in sys.stdin]

while not done:
    for sign, num in freq_list:
        if sign == "+":
            x += num
        elif sign == "-":
            x -= num
        else:
            raise ValueError
        
        freqs.update((x,))
        if freqs[x] == 2:
            print(x)
            done = True
            break