#!/usr/bin/env python3

import sys
from collections import Counter

twofers = 0
threefers = 0

for box in sys.stdin:
    counts = list(Counter(box).values())
    if 2 in counts:
        twofers += 1
    if 3 in counts:
        threefers += 1

print(twofers * threefers)